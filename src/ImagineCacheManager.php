<?php
namespace Avris\Micrus\Imagine;

use Avris\Dispatcher\EventSubscriberInterface;
use Avris\Micrus\Tool\Cache\CacheWarmupEvent;
use Avris\Micrus\Tool\DirectoryCleaner;

final class ImagineCacheManager implements EventSubscriberInterface
{
    /** @var ImagineManager */
    private $imagineManager;

    /** @var DirectoryCleaner */
    private $directoryCleaner;

    public function __construct(
        ImagineManager $imagineManager,
        DirectoryCleaner $directoryCleaner
    ) {
        $this->imagineManager = $imagineManager;
        $this->directoryCleaner = $directoryCleaner;
    }

    public function warmup(CacheWarmupEvent $event)
    {
        if (!$event->includeOptional()) {
            return;
        }

        $it = new \RecursiveDirectoryIterator($this->imagineManager->getSourcePath(), \RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new \RecursiveIteratorIterator($it, \RecursiveIteratorIterator::CHILD_FIRST);

        foreach ($files as $file) {
            if ($file->isDir()) {
                continue;
            }

            $this->imagineManager->generateAll($file->getPathName());
        }
    }

    public function clear()
    {
        $this->directoryCleaner->removeDir($this->imagineManager->getTargetPath());
    }

    /**
     * @codeCoverageIgnore
     */
    public function getSubscribedEvents(): iterable
    {
        yield 'cacheWarmup' => [$this, 'warmup'];
        yield 'cacheClear' => [$this, 'clear'];
    }
}
