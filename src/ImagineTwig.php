<?php
namespace Avris\Micrus\Imagine;

use Avris\Micrus\Controller\Routing\Service\RouterInterface;

final class ImagineTwig extends \Twig_Extension
{
    /** @var RouterInterface */
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('imagine', function ($filename, $generator) {
                return $this->router->generate('imagineGenerate', [
                    'generator' => $generator,
                    'filename' => $filename,
                ], false, '');
            }),
        ];
    }
}
