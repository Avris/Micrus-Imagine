<?php
/**
 * Based on https://github.com/liip/LiipImagineBundle/blob/2.0/Imagine/Filter/Loader
 * License attached in LICENSE.md
 */
namespace Avris\Micrus\Imagine\Filter;

/**
 * Upscale filter.
 *
 * @author Maxime Colin <contact@maximecolin.fr>
 * @author Devi Prasad <https://github.com/deviprsd21>
 */
final class UpscaleFilterLoader extends ScaleFilterLoader
{
    public function __construct()
    {
        parent::__construct('min', 'by', false);
    }

    protected function calcAbsoluteRatio($ratio)
    {
        return 1 + $ratio;
    }

    protected function isImageProcessable($ratio)
    {
        return $ratio > 1;
    }

    public function getName(): string
    {
        return 'upscale';
    }
}
