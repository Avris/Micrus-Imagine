<?php
/**
 * Based on https://github.com/liip/LiipImagineBundle/blob/2.0/Imagine/Filter/Loader
 * License attached in LICENSE.md
 */
namespace Avris\Micrus\Imagine\Filter;

use Imagine\Filter\Basic\Strip;
use Imagine\Image\ImageInterface;
use Imagine\Image\ImagineInterface;

final class StripFilterLoader implements LoaderInterface
{
    public function load(ImagineInterface $imagine, ImageInterface $image, array $options = array()): ImageInterface
    {
        $filter = new Strip();
        $image = $filter->apply($image);

        return $image;
    }

    public function getName(): string
    {
        return 'strip';
    }
}
