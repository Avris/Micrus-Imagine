<?php
/**
 * Based on https://github.com/liip/LiipImagineBundle/blob/2.0/Imagine/Filter/Loader
 * License attached in LICENSE.md
 */

namespace Avris\Micrus\Imagine\Filter;

use Imagine\Image\ImageInterface;
use Imagine\Image\ImagineInterface;

/**
 * Loader for Imagine's basic rotate method.
 *
 * @author Bocharsky Victor <bocharsky.bw@gmail.com>
 */
final class RotateFilterLoader implements LoaderInterface
{
    public function load(ImagineInterface $imagine, ImageInterface $image, array $options = array()): ImageInterface
    {
        $angle = (int) ($options['angle'] ?? 0);

        return 0 === $angle ? $image : $image->rotate($angle);
    }

    public function getName(): string
    {
        return 'rotate';
    }
}
