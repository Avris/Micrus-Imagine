<?php
/**
 * Based on https://github.com/liip/LiipImagineBundle/blob/2.0/Imagine/Filter/Loader
 * License attached in LICENSE.md
 */
namespace Avris\Micrus\Imagine\Filter;

use Imagine\Exception\InvalidArgumentException;
use Imagine\Image\ImageInterface;
use Imagine\Image\ImagineInterface;

/**
 * Loader for this bundle's relative resize filter.
 *
 * @author Jeremy Mikola <jmikola@gmail.com>
 */
final class RelativeResizeFilterLoader implements LoaderInterface
{
    public function load(ImagineInterface $imagine, ImageInterface $image, array $options = array()): ImageInterface
    {
        foreach ($options as $method => $parameter) {
            if (!in_array($method, ['heighten', 'increase', 'scale', 'widen'])) {
                throw new InvalidArgumentException(sprintf('Unsupported method: ', $method));
            }

            return $image->resize(call_user_func([$image->getSize(), $method], $parameter));
        }

        throw new InvalidArgumentException('Expected method/parameter pair, none given');
    }

    public function getName(): string
    {
        return 'relativeResize';
    }
}
