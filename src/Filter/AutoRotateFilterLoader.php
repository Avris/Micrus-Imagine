<?php
/**
 * Based on https://github.com/liip/LiipImagineBundle/blob/2.0/Imagine/Filter/Loader
 * License attached in LICENSE.md
 */
namespace Avris\Micrus\Imagine\Filter;

use Imagine\Image\ImageInterface;
use Imagine\Image\ImagineInterface;

/**
 * AutoRotateFilterLoader - rotates an Image based on its EXIF Data.
 *
 * @author Robert Schönthal <robert.schoenthal@gmail.com>
 */
final class AutoRotateFilterLoader implements LoaderInterface
{
    const ORIENTATION_KEYS = [
        'exif.Orientation',
        'ifd0.Orientation',
    ];

    public function load(ImagineInterface $imagine, ImageInterface $image, array $options = []): ImageInterface
    {
        if ($orientation = $this->getOrientation($image)) {
            if ($orientation < 1 || $orientation > 8) {
                return $image;
            }

            // Rotates if necessary.
            $degree = $this->calculateRotation($orientation);
            if ($degree !== 0) {
                $image->rotate($degree);
            }

            // Flips if necessary.
            if ($this->isFlipped($orientation)) {
                $image->flipHorizontally();
            }
        }

        return $image;
    }

    private function calculateRotation(int $orientation): int
    {
        switch ($orientation) {
            case 1:
            case 2:
                return 0;
            case 3:
            case 4:
                return 180;
            case 5:
            case 6:
                return 90;
            case 7:
            case 8:
                return -90;
        }
    }

    private function getOrientation(ImageInterface $image): ?int
    {
        //>0.6 imagine meta data interface
        if (method_exists($image, 'metadata')) {
            foreach (self::ORIENTATION_KEYS as $orientationKey) {
                $orientation = $image->metadata()->offsetGet($orientationKey);

                if ($orientation) {
                    $image->metadata()->offsetSet($orientationKey, '1');

                    return intval($orientation);
                }
            }
        } else {
            $data = exif_read_data('data://image/jpeg;base64,'.base64_encode($image->get('jpg')));

            return isset($data['Orientation']) ? $data['Orientation'] : null;
        }
    }

    private function isFlipped(int $orientation): bool
    {
        switch ($orientation) {
            case 1:
            case 3:
            case 6:
            case 8:
                return false;

            case 2:
            case 4:
            case 5:
            case 7:
                return true;
        }
    }

    public function getName(): string
    {
        return 'autoRotate';
    }
}
