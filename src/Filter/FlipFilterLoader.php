<?php
/**
 * Based on https://github.com/liip/LiipImagineBundle/blob/2.0/Imagine/Filter/Loader
 * License attached in LICENSE.md
 */
namespace Avris\Micrus\Imagine\Filter;

use Imagine\Image\ImageInterface;
use Imagine\Image\ImagineInterface;

final class FlipFilterLoader implements LoaderInterface
{
    public function load(ImagineInterface $imagine, ImageInterface $image, array $options = array()): ImageInterface
    {
        switch ($options['axis'] ?? null) {
            case 'x':
            case 'horizontal':
                return $image->flipHorizontally();
            case 'y':
            case 'vertical':
                return $image->flipVertically();
            default:
                throw new \InvalidArgumentException('The "axis" option must be set to "x", "horizontal", "y", or "vertical".');
        }
    }

    public function getName(): string
    {
        return 'flip';
    }
}
