<?php
/**
 * Based on https://github.com/liip/LiipImagineBundle/blob/2.0/Imagine/Filter/Loader
 * License attached in LICENSE.md
 */
namespace Avris\Micrus\Imagine\Filter;

use Imagine\Image\ImageInterface;
use Imagine\Image\ImagineInterface;
use Imagine\Image\Point;

final class PasteFilterLoader implements LoaderInterface
{
    /** @var string */
    private $rootPath;

    public function __construct(string $envProjectDir)
    {
        $this->rootPath = $envProjectDir;
    }

    public function load(ImagineInterface $imagine, ImageInterface $image, array $options = array()): ImageInterface
    {
        $x = $options['start'][0] ?? null;
        $y = $options['start'][1] ?? null;

        $destImage = $imagine->open($this->rootPath.'/'.$options['image']);

        return $image->paste($destImage, new Point($x, $y));
    }

    public function getName(): string
    {
        return 'paste';
    }
}
