<?php
/**
 * Based on https://github.com/liip/LiipImagineBundle/blob/2.0/Imagine/Filter/Loader
 * License attached in LICENSE.md
 */
namespace Avris\Micrus\Imagine\Filter;

/**
 * Downscale filter.
 *
 * @author Devi Prasad <https://github.com/deviprsd21>
 */
final class DownscaleFilterLoader extends ScaleFilterLoader
{
    public function __construct()
    {
        parent::__construct('max', 'by', false);
    }

    protected function calcAbsoluteRatio($ratio)
    {
        return 1 - ($ratio > 1 ? $ratio - floor($ratio) : $ratio);
    }

    protected function isImageProcessable($ratio)
    {
        return $ratio < 1;
    }

    public function getName(): string
    {
        return 'downscale';
    }
}
