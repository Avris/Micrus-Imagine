<?php
/**
 * Based on https://github.com/liip/LiipImagineBundle/blob/2.0/Imagine/Filter/Loader
 * License attached in LICENSE.md
 */
namespace Avris\Micrus\Imagine\Filter;

use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Image\ImagineInterface;
use Imagine\Image\Point;

final class BackgroundFilterLoader implements LoaderInterface
{
    public function load(ImagineInterface $imagine, ImageInterface $image, array $options = array()): ImageInterface
    {
        $background = $image->palette()->color(
            isset($options['color']) ? $options['color'] : '#fff',
            isset($options['transparency']) ? $options['transparency'] : null
        );
        $topLeft = new Point(0, 0);
        $size = $image->getSize();

        if (isset($options['size'])) {
            $width = isset($options['size'][0]) ? $options['size'][0] : null;
            $height = isset($options['size'][1]) ? $options['size'][1] : null;

            $position = isset($options['position']) ? $options['position'] : 'center';
            switch ($position) {
                case 'topleft':
                    $x = 0;
                    $y = 0;
                    break;
                case 'top':
                    $x = ($width - $image->getSize()->getWidth()) / 2;
                    $y = 0;
                    break;
                case 'topright':
                    $x = $width - $image->getSize()->getWidth();
                    $y = 0;
                    break;
                case 'left':
                    $x = 0;
                    $y = ($height - $image->getSize()->getHeight()) / 2;
                    break;
                case 'center':
                    $x = ($width - $image->getSize()->getWidth()) / 2;
                    $y = ($height - $image->getSize()->getHeight()) / 2;
                    break;
                case 'right':
                    $x = $width - $image->getSize()->getWidth();
                    $y = ($height - $image->getSize()->getHeight()) / 2;
                    break;
                case 'bottomleft':
                    $x = 0;
                    $y = $height - $image->getSize()->getHeight();
                    break;
                case 'bottom':
                    $x = ($width - $image->getSize()->getWidth()) / 2;
                    $y = $height - $image->getSize()->getHeight();
                    break;
                case 'bottomright':
                    $x = $width - $image->getSize()->getWidth();
                    $y = $height - $image->getSize()->getHeight();
                    break;
                default:
                    throw new \InvalidArgumentException("Unexpected position '{$position}'");
                    break;
            }

            $size = new Box($width, $height);
            $topLeft = new Point($x, $y);
        }

        $canvas = $imagine->create($size, $background);

        return $canvas->paste($image, $topLeft);
    }

    public function getName(): string
    {
        return 'background';
    }
}
