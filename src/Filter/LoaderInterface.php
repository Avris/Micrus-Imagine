<?php

namespace Avris\Micrus\Imagine\Filter;

use Imagine\Image\ImageInterface;
use Imagine\Image\ImagineInterface;

interface LoaderInterface
{
    public function load(ImagineInterface $imagine, ImageInterface $image, array $options = []): ImageInterface;

    public function getName(): string;
}
