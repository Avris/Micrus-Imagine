<?php
/**
 * Based on https://github.com/liip/LiipImagineBundle/blob/2.0/Imagine/Filter/Loader
 * License attached in LICENSE.md
 */
namespace Avris\Micrus\Imagine\Filter;

use Imagine\Image\ImageInterface;
use Imagine\Image\ImagineInterface;

final class InterlaceFilterLoader implements LoaderInterface
{
    public function load(ImagineInterface $imagine, ImageInterface $image, array $options = array()): ImageInterface
    {
        $mode = ImageInterface::INTERLACE_LINE;
        if (!empty($options['mode'])) {
            $mode = $options['mode'];
        }

        $image->interlace($mode);

        return $image;
    }

    public function getName(): string
    {
        return 'interlace';
    }
}
