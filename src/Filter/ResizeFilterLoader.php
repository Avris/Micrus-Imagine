<?php
/**
 * Based on https://github.com/liip/LiipImagineBundle/blob/2.0/Imagine/Filter/Loader
 * License attached in LICENSE.md
 */
namespace Avris\Micrus\Imagine\Filter;

use Imagine\Filter\Basic\Resize;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Image\ImagineInterface;

/**
 * Loader for Imagine's basic resize filter.
 *
 * @author Jeremy Mikola <jmikola@gmail.com>
 */
final class ResizeFilterLoader implements LoaderInterface
{
    public function load(ImagineInterface $imagine, ImageInterface $image, array $options = array()): ImageInterface
    {
        $width = $options['size'][0] ?? null;
        $height = $options['size'][1] ?? null;

        $filter = new Resize(new Box($width, $height));

        return $filter->apply($image);
    }

    public function getName(): string
    {
        return 'resize';
    }
}
