<?php
/**
 * Based on https://github.com/liip/LiipImagineBundle/blob/2.0/Imagine/Filter/Loader
 * License attached in LICENSE.md
 */
namespace Avris\Micrus\Imagine\Filter;

use Imagine\Filter\Advanced\Grayscale;
use Imagine\Image\ImageInterface;
use Imagine\Image\ImagineInterface;

/**
 * GrayscaleFilterLoader - apply grayscale filter.
 *
 * @author Gregoire Humeau <gregoire.humeau@gmail.com>
 */
final class GrayscaleFilterLoader implements LoaderInterface
{
    public function load(ImagineInterface $imagine, ImageInterface $image, array $options = array()): ImageInterface
    {
        $filter = new Grayscale();

        return $filter->apply($image);
    }

    public function getName(): string
    {
        return 'grayscale';
    }
}
