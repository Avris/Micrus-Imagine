<?php
namespace Avris\Micrus\Imagine;

use Avris\Micrus\Bootstrap\ConfigExtension;
use Avris\Micrus\Controller\ControllerInterface;
use Avris\Http\Response\Response;
use Avris\Micrus\Exception\ConfigException;
use Avris\Micrus\Exception\Http\BadRequestHttpException;
use Avris\Micrus\Exception\Http\NotFoundHttpException;
use Avris\Micrus\Imagine\Filter\LoaderInterface;
use Avris\Bag\Bag;
use Imagine\Image\AbstractImagine;
use Imagine\Image\ImageInterface;

final class ImagineManager implements ControllerInterface, ConfigExtension
{
    private static $supported = ['gif', 'jpeg', 'png', 'wbmp', 'xbm'];

    private static $mimeTypes = [
        'jpeg' => 'image/jpeg',
        'gif'  => 'image/gif',
        'png'  => 'image/png',
        'wbmp' => 'image/vnd.wap.wbmp',
        'xbm'  => 'image/xbm',
    ];

    /** @var AbstractImagine */
    private $imagine;

    /** @var bool */
    private $debug;

    /** @var string */
    private $sourcePath;

    /** @var string */
    private $target;

    /** @var string */
    private $targetPath;

    /** @var Bag */
    private $generators;

    /** @var Bag|LoaderInterface[] */
    private $filters;

    public function __construct(
        Bag $configImagine,
        bool $envAppDebug,
        string $envPublicDir,
        array $imagineFilters
    ) {
        $imagineClass = sprintf('\Imagine\%s\Imagine', $configImagine->get('driver'));
        $this->imagine = new $imagineClass;

        $this->debug = $envAppDebug;
        $this->sourcePath = $configImagine->get('source');
        $this->target = $configImagine->get('target') . ($this->debug ? '/debug' : '');
        $this->targetPath = $envPublicDir . '/' . $this->target;
        $this->generators = new Bag($configImagine->get('generators', []));

        $this->filters = new Bag;
        foreach ($imagineFilters as $filter) {
            $this->filters[$filter->getName()] = $filter;
        }
    }

    public function generateAction(string $generatorName, string $filename)
    {
        $path = $this->sourcePath . '/' . $filename;
        if (!file_exists($path)) {
            throw new NotFoundHttpException(sprintf('File "%s" does not exist', $path));
        }

        $generator = $this->generators->get($generatorName);
        if ($generator === null) {
            throw new NotFoundHttpException(sprintf('Generator "%s" not found', $generatorName));
        }

        $image = $this->imagine->open($path);

        $format = $this->normalizeFormat($this->getExtension($filename));
        if (!in_array($format, self::$supported)) {
            throw new BadRequestHttpException(sprintf('Format "%s" is not supported', $format));
        }
        $formatOptions = [];

        foreach ($generator as $filterName => $filterOptions) {
            if ($filterName === 'output') {
                if (isset($filterOptions['format'])) {
                    $format = $this->normalizeFormat($filterOptions['format']);
                    unset($filterOptions['format']);
                }
                $formatOptions = $filterOptions;
                continue;
            }

            $image = $this->applyFilter($image, $filterName, $filterOptions);
        }

        $generated = $image->get($format, $formatOptions);

        if (!$this->debug) {
            $path = $this->targetPath . '/' . $generatorName . '/' . $filename;
            if (!file_exists($dir = dirname($path))) {
                mkdir($dir, 0777, true);
            }

            file_put_contents($path, $generated);
        }

        return new Response($generated, 200, ['content-type' => self::$mimeTypes[$format]]);
    }

    private function applyFilter(ImageInterface $image, string $name, array $options): ImageInterface
    {
        $filter = $this->filters->get($name);

        if (!$filter) {
            throw new ConfigException(sprintf('Undefined filter "%s"', $name));
        }

        $prevImage = $image;
        $image = $filter->load($this->imagine, $image, $options);

        if ($prevImage !== $image && method_exists($prevImage, '__destruct')) {
            $prevImage->__destruct();
        }

        return $image;
    }

    public function generateAll(string $filename)
    {
        try {
            $relative = substr($filename, strlen($this->sourcePath) + 1);
            foreach ($this->generators->keys() as $generator) {
                $this->generateAction($generator, $relative);
            }
        } catch (\Exception $e) {}
    }

    private function getExtension($filename)
    {
        return preg_match('/\.([^\/\\\\\.]*)/i', $filename, $matches) ? $matches[1] : '';
    }

    private function normalizeFormat($format)
    {
        $format = strtolower($format);

        if ('jpg' === $format || 'pjpeg' === $format) {
            $format = 'jpeg';
        }

        return $format;
    }

    public function getImagine()
    {
        return $this->imagine;
    }

    public function getSourcePath(): string
    {
        return $this->sourcePath;
    }

    public function getTargetPath(): string
    {
        return $this->targetPath;
    }

    /**
     * @codeCoverageIgnore
     */
    public function extendConfig(): array
    {
        return [
            'routing' => [
                'imagineGenerate' => sprintf(
                    'GET /%s/{generator}/{path:filename} -> %s/generate {"immutable":true}',
                    $this->target,
                    __CLASS__
                ),
            ],
        ];
    }
}
