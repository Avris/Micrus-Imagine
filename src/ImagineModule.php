<?php
namespace Avris\Micrus\Imagine;

use Avris\Micrus\Bootstrap\ModuleInterface;
use Avris\Micrus\Bootstrap\ModuleTrait;

class ImagineModule implements ModuleInterface
{
    use ModuleTrait;
}
