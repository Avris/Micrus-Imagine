# Micrus Imagine

This is a module for [Micrus framework](https://micrus.avris.it) that allows you
to integrate it with [Imagine](https://imagine.readthedocs.org/en/latest/)
in a similar way that [LiipImagineBundle](https://github.com/liip/LiipImagineBundle) does in Symfony.

This module lets you define image generators that will automatically apply a set of filters to the specified images and cache the result.

## Installation

Run:

    composer require avris/micrus-imagine

Then register the module in your `App\App:registerModules`: 

    yield new \Avris\Micrus\Imagine\ImagineModule;

## Usage

Let's say you want to allow users to attach images to their posts. Whatever they upload, you will save the originals
in `var/attachments/` folder, but you only want to display it as a 200x200 thumbnail with a watermark in the bottom left corner.

Configure the module in the `config/imagine.yml`, for instance like that:

    source: '%PROJECT_DIR%/var/attachments' # default: '%PROJECT_DIR%/var/pics'
    generators:
      thumb:
        thumbnail: { size: [200, 200], mode: outbound }
        watermark: { image: assets/images/logo.png, size: 10%, position: bottomright }

You created a `thumb` generator that applies two filteres to the original image: `thumbnail` and `watermark`.

Now, wherever you wish to display the thumbnail, put such code there:

    <img src="{{ post.file|imagine('thumb') }}"/>

It will generate a URL which will point either to a generated image (if it already exists) or to a controller that will generate it.

The built-in list of filters is copied from [LiipImagineBundle](https://symfony.com/doc/master/bundles/LiipImagineBundle/filters.html) (MIT).

## Extending the module

To create your own filter, just implement `Avris\Micrus\Imagine\Filter\LoaderInterface` (`imagineFilter`).

## Copyright

* **Author:** Andre Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
